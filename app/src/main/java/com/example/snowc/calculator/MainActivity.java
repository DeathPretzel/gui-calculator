package com.example.snowc.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public double X = 0;
    public double Y = 0;
    public boolean reset = false;
    public boolean first = true;
    public boolean nodec = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button equals = findViewById(R.id.equals);
        Button zero = findViewById(R.id.zero);
        Button one = findViewById(R.id.one);
        Button two = findViewById(R.id.two);
        Button three = findViewById(R.id.three);
        Button four = findViewById(R.id.four);
        Button five = findViewById(R.id.five);
        Button six = findViewById(R.id.six);
        Button seven = findViewById(R.id.seven);
        Button eight = findViewById(R.id.eight);
        Button nine = findViewById(R.id.nine);
        Button minus = findViewById(R.id.minus);
        Button plus = findViewById(R.id.add);
        Button times = findViewById(R.id.multiply);
        Button divide = findViewById(R.id.divide);
        Button expon = findViewById(R.id.expon);
        Button root = findViewById(R.id.root);
        Button yroot = findViewById(R.id.yroot);
        Button parenth = findViewById(R.id.parenth);
        Button bracket = findViewById(R.id.bracket);
        Button toggle = findViewById(R.id.toggle);
        Button sin = findViewById(R.id.sin);
        Button tan = findViewById(R.id.tan);
        Button cos = findViewById(R.id.cos);
        Button point = findViewById(R.id.decimal);
        Button clear = findViewById(R.id.clear);

        final TextView display = (TextView) findViewById(R.id.display);

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText("0");
                first = true;
                nodec = true;
            }
        });

        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button)view;
                String value = b.getText().toString();
                if(first) {
                    display.setText(value);
                    first = !first;
                } else {
                    display.append(value);
                }
            }
        });

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button)view;
                String value = b.getText().toString();
                if(first) {
                    display.setText(value);
                    first = !first;
                } else {
                    display.append(value);
                }
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button)view;
                String value = b.getText().toString();
                if(first) {
                    display.setText(value);
                    first = !first;
                } else {
                    display.append(value);
                }
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button)view;
                String value = b.getText().toString();
                if(first) {
                    display.setText(value);
                    first = !first;
                } else {
                    display.append(value);
                }
            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button)view;
                String value = b.getText().toString();
                if(first) {
                    display.setText(value);
                    first = !first;
                } else {
                    display.append(value);
                }
            }
        });

        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button)view;
                String value = b.getText().toString();
                if(first) {
                    display.setText(value);
                    first = !first;
                } else {
                    display.append(value);
                }
            }
        });

        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button)view;
                String value = b.getText().toString();
                if(first) {
                    display.setText(value);
                    first = !first;
                } else {
                    display.append(value);
                }
            }
        });

        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button)view;
                String value = b.getText().toString();
                if(first) {
                    display.setText(value);
                    first = !first;
                } else {
                    display.append(value);
                }
            }
        });

        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button)view;
                String value = b.getText().toString();
                if(first) {
                    display.setText(value);
                    first = !first;
                } else {
                    display.append(value);
                }
            }
        });

        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button)view;
                String value = b.getText().toString();
                if(first) {
                    display.setText(value);
                    first = !first;
                } else {
                    display.append(value);
                }
            }
        });

        point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button)view;
                String value = b.getText().toString();
                if(nodec) {
                    display.append(value);
                    nodec = !nodec;
                }
            }
        });
    }
}
